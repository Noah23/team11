<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    protected $fillable = [
        'eventname'
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Register');
    }
}
