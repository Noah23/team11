<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinalRankings extends Model
{
    protected $fillable = [
        'score', 'weight', 'playerId', 'eventId'
    ];

}
