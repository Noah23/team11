<?php

namespace App\Http\Controllers;

use App\Http\Requests\searchPlayerRequest;
use App\PlayerScore;
use Illuminate\Http\Request;
use App\Register;
use App\Events;
use Crypt;
use Auth;
use Illuminate\Support\Facades\DB;
use PlayerScoreController;

class RegistrationController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $events = Events::all();

        return view('registration', compact('events'));
    }

    public function store(Request $request) {

        $request->validate([
            'name' => 'required|string',
            'phone' => 'required',
            'age' => 'required|integer|min:10|max:99',
            'dropdown' => 'required|string'
        ]);


        $register = new Register();
        $name = request('name');
        $number = request('phone');
        $address = request('address');
        $age = request('age');
        $event = request('dropdown');



        $numResults = Register::where('playerId', Auth::user()->id)->count();

        if ($numResults >= 5) {
            $error = "Je mag maximaal 5 inschrijvingen hebben";
            return redirect('/registers')->with('error', $error);
        }


        if ($age < 10) {
            $error = "You moet minstens 10 jaar zijn om mee te doen.";
            return redirect('/registers')->with('error', $error);
        }

        $register->name = $name;
        $register->number = $number;
        $register->address = $address;
        $register->age = $age;
        $register->playerId = Auth::user()->id;
        $register->eventId = $event;

        $register->save();

        $playerScore = new PlayerScore();


        $playerScore->playerId = $register->id;

        $playerScore->eventId = $event;

        $playerScore->save();


        return redirect('/registeredUsers');
    }

    public function showRegistered() {
        $registered = Register::paginate(15);
        return view('registeredUsers')->with('registered', $registered);
    }

    public function reset() {
        $registered = Register::truncate();
        $playerScore = PlayerScore::truncate();
        return redirect('/registeredUsers');
    }

    public function removeUser(Request $request) {
        if (Auth::user()->type != 3) {
            abort(403, 'Unauthorized action');
        }

        $playerId = Crypt::decrypt($request->playerId);
        $registerId = Crypt::decrypt($request->id);


        $register = Register::findOrFail($registerId);
        $playerScore = PlayerScore::where('playerId', '=', $registerId)->firstOrFail();
        $playerScore->delete();
        $register->delete();

        return redirect('/registeredUsers');
    }

    public function unsubscribe(Request $request) {

        $playerId = Crypt::decrypt($request->playerId);
        $registerId = Crypt::decrypt($request->id);
        dd($playerId);

        if (Auth::user()->id != $playerId) {
            abort(403, 'Unauthorized action');
        }
        $register = Register::findOrFail(Crypt::decrypt($request->id));
        $playerScore = PlayerScore::where('playerId', '=', $registerId)->firstOrFail();
        $playerScore->delete();
        $register->delete();

        $message = "met succes uitgeschreven";

        return redirect('/profile')->with('message', $message);
    }

    public function searchPlayer(searchPlayerRequest $request) {
        $idRequest = $request->validated();

//        dd($idRequest);

        $id = $idRequest['search'];

        $registered = DB::table('registers')
            ->select('*')
            ->where('id', 'LIKE', '%' . $id . '%')->get();

        return view('/registeredUsers', compact('registered'));
    }
}
