<?php

namespace App\Http\Controllers;

use App\Confirm;
use App\Register;
use Illuminate\Http\Request;
class ConfirmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $confirm = Confirm::all();
        $registered = Register::paginate(15);
        return view('confirm', compact('confirm', 'registered'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('confirm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Confirm;

        $event->confirm = true;
        $event->save();

        return redirect()->route('confirm');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function show(Confirm $confirm)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function edit(Confirm $confirm)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Confirm $confirm)
    {
        $event = new Confirm;

        $event->confirm = true;
        $event->save();

        return redirect()->route('confirm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Confirm  $confirm
     * @return \Illuminate\Http\Response
     */
    public function destroy(Confirm $confirm)
    {
        //
    }
}
