<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Crypt;
use App\User;
use App\Events;

class AdminController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $users = User::paginate(15);
        $events = Events::paginate(15);

        return view('adminPanel')->with(['users' => $users, 'events' => $events]);

    }

    public function searchUser(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $term = $request->search;

        $search = User::where("name", "LIKE", "%" . $term . "%")->orWhere("email", "LIKE", "%" . $term . "%")->get();
        return redirect('adminPanel')->with(['search' => $search, 'term' => $term]);
    }

    public function makeAdmin(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $id = $request->id;
        $user = User::find($id);
        $user->type = 3;
        $user->save();

        return redirect('/adminPanel');
    }

    public function makeEmployee(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $id = $request->id;
        $user = User::find($id);
        $user->type = 2;
        $user->save();

        return redirect('/adminPanel');
    }

    public function makeUser(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $id = $request->id;
        $user = User::find($id);
        $user->type = 0;
        $user->save();

        return redirect('/adminPanel');
    }

    public function removeUser(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $user = User::findOrFail($request->id);
        $user->delete();

        return redirect('/adminPanel');
    }

    public function updateName(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $user = User::findOrFail($request->id);
        $user->name = $request->name;
        $user->save();

        return redirect('/adminPanel');
    }

    public function updateEmail(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $user = User::findOrFail($request->id);
        $user->email = $request->email;
        $user->save();

        return redirect('/adminPanel');
    }

    public function removeEvent(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $events = Events::findOrFail($request->id);
        $events->delete();

        return redirect('/adminPanel?cre=1');
    }

    public function updateEvent(Request $request)
    {
        if (Auth::user()->type != 3) {
            abort(403);
        }

        $events = Events::findOrFail($request->id);
        $events->eventname = $request->name;
        $events->save();

        return redirect('/adminPanel?cre=1');
    }


}
