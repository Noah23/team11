<?php

namespace App\Http\Controllers;

use App\Events;
use App\Http\Requests\UpdatePlayerScoreRequest;
use App\PlayerScore;
use App\Register;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PlayerScoreController extends Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {
        $events = Events::all();

        return view('events', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UpdatePlayerScoreRequest $request) {
//        dd('yeeet');
        $data = $request->validated();
        $playerScore = new PlayerScore();


    }

    /**
     * Display the specified resource.
     *
     * @param CreateEvent $event
     * @return \Illuminate\Http\Response
     */
    public function show(Events $event) {
        $playerScores = PlayerScore::join('registers', 'player_scores.playerId', '=', 'registers.id')
            ->select('player_scores.*', 'registers.*')->where('registers.eventId', '=', $event->id)->orderBy('player_scores.weight', 'DESC')->get();

        return view('rankingspage', compact('playerScores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\PlayerScore $playerScore
     * @return \Illuminate\Http\Response
     */
    public function edit($register) {
        return view('editScore')->with(['id' => $register]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\PlayerScore $playerScore
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlayerScoreRequest $request) {
        $data = $request->validated();
        $playerScore = PlayerScore::find($data['playerId']);
        $playerScore->score = $playerScore->score + $data['score'];

        $playerScore->save();

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\PlayerScore $playerScore
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlayerScore $playerScore) {
        //
    }
}
