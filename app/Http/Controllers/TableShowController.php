<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayerScore;
use App\Register;
use App\Events;
use Illuminate\Support\Facades\DB;

class TableShowController extends Controller {

    public $id1 = 0;

    public function index($id) {
        // we get the ID from the URL
        $eventId = $id;
        $this->id1 = $id;

        // here we get the rows that have the eventId of the $ID
//        $scores = PlayerScore::join('registers', 'player_scores.playerId', '=', 'registers.id')
//        ->where("player_scores.eventId", "LIKE", $eventId )->get();

        $scores = DB::table('player_scores')
            ->where('player_scores.eventId', 'LIKE', $eventId)
            ->join('registers', 'player_scores.playerId', '=', 'registers.id')
            ->select('player_scores.*', 'registers.playerId as regPlayerId', 'registers.name')->get();


        // $table = PlayerScore::where("eventId", $eventId )->pluck('table_number')->first();
        $people = Register::where("eventId", $eventId)->get();

        // here we grab the amount of unique  tables
        $tableAmt = PlayerScore::where("eventId", $eventId)->distinct('tableNumber')->count();

        $players = PlayerScore::where("eventId", "LIKE", $eventId)->get();

        return view('tableShow', compact('scores', 'players', 'people', 'tableAmt'));
    }

    public function calculate(Request $request) {
        $players = PlayerScore::join('events', 'player_scores.eventId', '=', 'events.id')
            ->where("tableNumber", "LIKE", $request->input('tableId'))->get();


        $tableTotal = 0;
        foreach ($players as $player) {
            $playerScore = request('score' . $player->playerId);
//            $inputScore = $request->input($inputScore);

            $tableTotal = $tableTotal + $playerScore;
        }


        foreach ($players as $player) {

//            $tournamentRound = $player->tournamentRound;
            $playerScore = request('score' . $player->playerId);
//            $inputScore = 'score' . $player->playerId;
//            $playerScore = $request->input($inputScore);
            $playerId = $player->playerId;


            // we get the event to check what round
//            $event = Events::where("id", $this->id1 )->get();


            if ($player->tournamentRound != 1) {

                if ($player->tournamentRound > 1) {
                    $gameWeight = ($playerScore / $tableTotal) * 100;
                    $player->totalScore += $gameWeight;
                    PlayerScore::where("playerId", $playerId)->update([
                        'totalScore' => $player->totalScore
                    ]);
                    $totalWeight = ($player->totalScore / $player->tournamentRound);
                    // $nextRound = $player->tournamentRound + 1;
                    $score = PlayerScore::where("playerId", $playerId)->update([
                        'weight' => $totalWeight
                    ]);


                    // return redirect()->back();


                } elseif ($player->tournamentRound == 6) {
                    return redirect("final/1");
                }

            } else {

                $gameWeight = ($playerScore / $tableTotal) * 100;
                $player->totalScore += $gameWeight;
                $totalWeight = $gameWeight;

                PlayerScore::where("playerId", $playerId)->update([
                    'totalScore' => $player->totalScore
                ]);

                $score = PlayerScore::where("playerId", $playerId)->update([
                    'weight' => $totalWeight
                ]);
            }
            $score = PlayerScore::where("playerId", $playerId)->update(['score' => $playerScore]);

        }
        return redirect()->back();

    }
}

