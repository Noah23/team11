<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Register;
use Illuminate\Support\Facades\DB;
use Auth;

class ProfileController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $results = DB::table('registers')
            ->where('playerId', Auth::user()->id)
            ->join('events', 'registers.eventId', '=', 'events.id')
            ->select('registers.*', 'events.id as eventId', 'events.eventname')->get();

        // $results = DB::table('registers')->where('accountId', Auth::user()->id)->get();
        return view('profile')->with('results', $results);
    }
}
