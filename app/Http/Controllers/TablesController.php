<?php

namespace App\Http\Controllers;

use App\Events;
use App\PlayerScore;
use Illuminate\Http\Request;

class TablesController extends Controller {
    public function calculateTables(Events $event) {

        if ($event->tournamentRound == 0) {
            $playerScores = PlayerScore::where('eventId', '=', $event->id)->get();

            $tableNumber = 1;

            for ($i = 0; $i < sizeof($playerScores); $i++) {
                if ($i > 0) {
                    if ($i % 4 == 0) {
                        $tableNumber++;
                    }
                }
                $playerScores[$i]->tableNumber = $tableNumber;
                $playerScores[$i]->save();
            }

            if (sizeof($playerScores) > 4) {


                $amountOnLastTable = PlayerScore::where('player_scores.tableNumber', '=', $tableNumber)->get();

                if (sizeof($playerScores) == 5) {
                    $playerScores[sizeof($playerScores) - 2]->tableNumber = $tableNumber;

                    $playerScores[sizeof($playerScores) - 2]->save();
                } elseif (sizeof($playerScores) == 6) {
                    $playerScores[sizeof($playerScores) - 3]->tableNUmber = $tableNumber;
                    $playerScores[sizeof($playerScores) - 3]->save();
                } else {

                    if (sizeof($amountOnLastTable) == 1) {
                        $playerScores[sizeof($playerScores) - 2]->tableNumber = $tableNumber;

                        $playerScores[sizeof($playerScores) - 2]->save();

                        $playerScores[sizeof($playerScores) - 6]->tableNumber = $tableNumber;
                        $playerScores[sizeof($playerScores) - 6]->save();

                    } elseif (sizeof($amountOnLastTable) == 2) {
                        $playerScores[sizeof($playerScores) - 3]->tableNumber = $tableNumber;
                        $playerScores[sizeof($playerScores) - 3]->save();
                    }
                }

            }
        } else {

            $playerScores = PlayerScore::where('eventId', '=', $event->id)->orderBy('player_scores.weight', 'DESC')->get();


            $tableNumber = 1;

            for ($i = 0; $i < sizeof($playerScores); $i++) {
                if ($i > 0) {
                    if ($i % 4 == 0) {
                        $tableNumber++;
                    }
                }
                $playerScores[$i]->tableNumber = $tableNumber;
                $playerScores[$i]->save();
            }

            if (sizeof($playerScores) > 4) {


                $amountOnLastTable = PlayerScore::where('player_scores.tableNumber', '=', $tableNumber)->get();

                if (sizeof($playerScores) == 5) {
                    $playerScores[sizeof($playerScores) - 2]->tableNumber = $tableNumber;

                    $playerScores[sizeof($playerScores) - 2]->save();
                } elseif (sizeof($playerScores) == 6) {
                    $playerScores[sizeof($playerScores) - 3]->tableNUmber = $tableNumber;
                    $playerScores[sizeof($playerScores) - 3]->save();
                } else {

                    if (sizeof($amountOnLastTable) == 1) {
                        $playerScores[sizeof($playerScores) - 2]->tableNumber = $tableNumber;

                        $playerScores[sizeof($playerScores) - 2]->save();

                        $playerScores[sizeof($playerScores) - 6]->tableNumber = $tableNumber;
                        $playerScores[sizeof($playerScores) - 6]->save();

                    } elseif (sizeof($amountOnLastTable) == 2) {
                        $playerScores[sizeof($playerScores) - 3]->tableNumber = $tableNumber;
                        $playerScores[sizeof($playerScores) - 3]->save();
                    }
                }

            }
        }
        $event->tournamentRound++;
        $event->save();

        return redirect()->back();
    }
}
