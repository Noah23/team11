<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayerScore;
use App\Register;
use App\Events;
use App\FinalRankings;
use Illuminate\Support\Facades\DB;

class FinalController extends Controller
{

    public function index($id)
    {

//        $ranking = FinalRankings::where("eventId", $id)->get();
        $ranking = FinalRankings::join('registers', 'final_rankings.playerId', '=', 'registers.playerId')->where('registers.eventId' , '=', $id)->get();
        $event = Events::where('id', $id)->get();
        $tableAmt = FinalRankings::where("eventId", $id)->Where('inComp', 1)->distinct('table')->count();


        return view('finalTables', compact('ranking', 'tableAmt'));
    }

    public function storeTop($id) {

        $players = PlayerScore::where("eventId", $id)->orderBy("weight", 'DESC')->take(16)->get();

        foreach ($players as $player) {
            $rankings = new FinalRankings([
                'weight' => $player->weight,
                'playerId' => $player->playerId,
                'score' => 0,
                'round' => 0,
                'eventId' => $player->eventId
                ]);

            $rankings->save();
        }
        return redirect("/finalTables/" . $id);
    }

    public function calculate($id) {
        $players = FinalRankings::where("eventId", $id)->get();
        $number = 0;
        $table = 1;

            foreach ($players as $player) {

                if ($player->inComp == 0) { continue;}

                if ($number % 2 == 0) {
                    $user = FinalRankings::find($player->id);
                    $user->playerNumber = 1;
                    $user->table = $table;
                    $user->save();
                } else {
                    $user = FinalRankings::find($player->id);
                    $user->playerNumber = 2;
                    $user->table = $table;
                    $user->save();
                    $table++;
                }
                $number += 1;
            }

            return redirect('finalTables/' . $id);
    }

    public function showTables($id) {
        //$ranking = FinalRankings::where("eventId", $id)->get();
        //$ranking = FinalRankings::join('registers', 'final_rankings.playerId', '=', 'registers.playerId')->where('registers.eventId' , '=', $id)->get();
        $tableAmt = FinalRankings::where("eventId", $id)->where('inComp', 1)->distinct('table')->count();
//        $registers = Register::where("eventId", $id)->get();

        $ranking = DB::table('final_rankings')
            ->where('registers.eventId', '=', $id)
            ->join('registers', 'final_rankings.playerId', '=', 'registers.id')
            ->select('registers.id as regId', 'registers.name', 'final_rankings.*')->get();


//        $results = DB::table('registers')
//            ->where('playerId', Auth::user()->id)
//            ->join('events', 'registers.eventId', '=', 'events.id')
//            ->select('registers.*', 'events.id as eventId', 'events.eventname')->get();

        return view('finalTables', compact('ranking', 'tableAmt'));
    }

    public function store(Request $request) {

        $players = FinalRankings::where("table","LIKE", $request->input('tableId'))->where("inComp", true)->get();

        $playerScores = array();

        foreach ($players as $player){
            $inputScore = 'score' . $player->id;
            $playerScores[] = $request->input($inputScore);
            $playerScores[] = $player->id;
        }


        // dd($playerScores);
        if ($playerScores[0] > $playerScores[2]) {
            echo "player 1 is the winner <br>";

            $user = FinalRankings::find($playerScores[1]);
            $user->inComp = true;
            $user->tournamentRound = $user->tournamentRound + 1;

            if ($user->tournamentRound == 2) {
                $user->round8 = true;
            } else if ($user->tournamentRound == 3) {
                $user->round4 = true;
            } else if ($user->tournamentRound == 4) {
                $user->round2 = true;
            } else if ($user->tournamentRound == 5) {
                $user->round1 = true;
            }
            $user->score = $user->score + $playerScores[0];
            $user->save();


            $user = FinalRankings::find($playerScores[3]);
            $user->inComp = false;
            $user->score = $user->score + $playerScores[2];
            $user->save();

            $count = FinalRankings::where("inComp", 1)->count();


            if ($count == 1) {
                $winner = FinalRankings::where("inComp", 1)->get();
                return redirect()->back()->with(['winner' => $winner]);
            }
            return redirect()->back();
        } else {
            echo "player 2 is the winner <br>";
            $user = FinalRankings::find($playerScores[3]);
            $user->inComp = true;

            if ($user->tournamentRound == 2) {
                $user->round8 = true;
            } else if ($user->tournamentRound == 3) {
                $user->round4 = true;
            } else if ($user->tournamentRound == 4) {
                $user->round2 = true;
            } else if ($user->tournamentRound == 5) {
                $user->round1 = true;
            }
            $user->score = $user->score + $playerScores[2];
            $user->save();

            $user = FinalRankings::find($playerScores[1]);
            $user->score = $user->score + $playerScores[3];
            $user->inComp = false;
            $user->save();

            $count = FinalRankings::where("inComp", 1)->count();

            if ($count == 1) {
                $winner = FinalRankings::where("inComp", 1)->get();
                return redirect()->back()->with(['winner' => $winner]);
            }

            return redirect()->back();
        }

    }
}
