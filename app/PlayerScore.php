<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerScore extends Model
{
    public function register() {
        $this->belongsTo(Register::class);
    }
}
