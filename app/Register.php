<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    public function playerScore() {
        $this->hasOne(PlayerScore::class);
    }
    public function events()
    {
        return $this->hasMany('App\Events');
    }
}
