<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//profile
Route::get('/profile', 'ProfileController@index');
//registration
Route::get('/registeredUsers', 'RegistrationController@showRegistered');
Route::get('/registers', 'RegistrationController@index');
Route::post('/registerEvent', 'RegistrationController@store');
Route::delete('/resetRegistration', 'RegistrationController@reset');
Route::delete('/removeFromRegister', 'RegistrationController@removeUser');
Route::delete('/unsubscribe', 'RegistrationController@unsubscribe');

// ranking
//Route::get('/input', 'RankingsController@input');
//Route::post('/rankings', 'RankingsController@output');
//Route::post('/input', 'RankingsController@input');

Route::get('/events', 'PlayerScoreController@index')->name('events');
Route::get('/rankings/{event}', 'PlayerScoreController@show')->name('rankings');
Route::get('/editScore', 'PlayerScoreController@edit')->name('editScore');
Route::put('/updateScore', 'PlayerScoreController@update')->name('updateScore');

// Tables

Route::get('/tables/{event}', 'TablesController@calculateTables')->name('tables');


//admin
Route::get('/adminPanel', 'AdminController@index');
Route::post('/searchUser', 'AdminController@searchUser');
Route::put('/makeAdmin', 'AdminController@makeAdmin');
Route::put('/makeEmployee', 'AdminController@makeEmployee');
Route::delete('/removeUser', 'AdminController@removeUser');
Route::put('/makeUser', 'AdminController@makeUser');
Route::put('/updateName', 'AdminController@updateName');
Route::put('/updateEmail', 'AdminController@updateEmail');
Route::delete('/removeEvent', 'AdminController@removeEvent');
Route::put('/updateEvent', 'AdminController@updateEvent');


//table show controller
Route::get('/edit/{id}', 'TableShowController@index');
Route::post('/edit', 'TableShowController@calculate');


// final
Route::get("/final/{eventId}", "FinalController@index");
Route::get("/finalTop/{eventId}", "FinalController@storeTop");
Route::get("/finalCalc/{eventId}", "FinalController@calculate");
Route::get("finalTables/{id}", "FinalController@showTables");
Route::patch("/finalWin", "FinalController@store");


// create event
Route::post("/createEvent", "EventsController@store");
