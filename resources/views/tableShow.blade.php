@extends('layouts.app')

@section('content')

    <div class="container">
        <div style="width: 100px; color:white;">
            <button class="btn btn-success" onclick="start()">Click me</button>
            Time left: <span id="timer">00:00</span>
        </div>
        <div class="row">
            @for($i = 1; $i <= $tableAmt; $i++)
                <div class="col-4 pb-3">
                    <div class="card">
                        <div class="card-header">
                            Table {{$i }}
                        </div>
                        <div class="card-body">


                            Table {{$i }}
                            @foreach ($scores as $score)

                                @if ($score->tableNumber == $i)
                                    @php

                                        $current = "close" . $score->tableNumber;
                                        if (isset($close)) {

                                            if ($close == $current) {
                                                continue;
                                            }
                                        }
                                    @endphp

                                    @php
                                        $playerNum = 1;
                                    @endphp
                                    <form action="{{ url("/edit") }}" method="POST">
                                        @csrf

                                        <div class="col-12 form-group">
                                            <h4>Player Number: {{$playerNum}}</h4>


                                            <h4><i>{{$score->name}}</i></h4>
                                            <h4>Jou totale punten: {{$score->score}}</h4>

                                        </div>
                                        @php
                                            $playerNum++;
                                        @endphp
                                        <div class="col-7 form-group">
                                            <input type="number" class="form-control"
                                                   name="score{{$score->playerId}}">


                                            <input type="hidden" name="tableId" value="{{$score->tableNumber}}">
                                            <input type="hidden" name="{{$score->playerId}}"
                                                   value="{{$score->playerId}}">
                                        </div>

                                        @endif
                                        @endforeach
                                        <div class="col-7">
                                            <input type="submit" name="yeeeeeeeeet" class="btn btn-primary"><br>
                                        </div>
                                    </form>
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </div>


@endsection


