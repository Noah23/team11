@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Edit Scores
                    </div>
                    <form action="{{ route('updateScore') }}" id="id" method="post">
                        @csrf
                        @method('put')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="tableNumber">Tafelnummer</label>
                                <input type="text" class="form-control" name="tableNumber" id="tableNumber"
                                       placeholder="Voer tafelnummer in">
                            </div>
                            <div class="form-group">
                                <label for="score">Score</label>
                                <input type="number" class="form-control" name="score" id="score"
                                       placeholder="Vul score in">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="playerId" value="{{ $id }}">
                                <button type="submit" class="btn btn-primary">submit scores</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
