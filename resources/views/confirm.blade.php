@extends('layouts.app')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p style="float: left">Gebruikers aanmelden</p>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{session('error')}}</li>
                                </ul>
                            </div>
                        @endif
                        <table style="width:100%">
                            <tr>
                                <th>AccountID</th>
                                <th>Naam</th>
                                <th>Leeftijd</th>
                                <th>Telefoon</th>
                                <th>Aanwezig</th>
                                <th>Event</th>
                            </tr>
                            @foreach($registered as $register)
                                <tr>
                                    <td>{{$register->accountId}}</td>
                                    <td>{{$register->name}}</td>
                                    <td>{{$register->age}}</td>
                                    <td>{{$register->number}}</td>
                                    <td>
                                        <!-- Modal -->
                                        <div style="text-align: center;">
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmPlayer">
                                                X
                                            </button>
                                        </div>
                                        <div class="modal" id="confirmPlayer" tabindex="-1" role="dialog" aria-labelledby="confirmPlayer" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="confirmPlayer">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Wilt u deze gebruiker <b>{{$register->name}}</b> aanmelden bij het event?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                        <form action="confirm" method="post">
                                                            @csrf
                                                            @method('put')
                                                            <input type="hidden" name="id" value="{{Crypt::encrypt($register->id)}}">
                                                            <center><input type="submit" value="Confirm" class="btn btn-danger"></center>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div id="pagination" style="text-align: center">
                            <div id="bar">
                                {{ $registered->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
