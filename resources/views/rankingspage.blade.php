@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-2">
                <div class="card">
                    <div class="card-header">
                        Rankings
                    </div>
                    <div class="card-body">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-2">
                                    <h5 style="font-weight: bold">ID</h5>
                                </div>
                                <div class="col-2">
                                    <h5 style="font-weight: bold">Name</h5>
                                </div>
                                <div class="col-2">
                                    <h5 style="font-weight: bold">Weight</h5>
                                </div>
                                <div class="col-2">
                                    <h5 style="font-weight: bold">Score</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 justify-content-center">
                            @foreach($playerScores as $playerScore)
                                <div class="row">
                                    <div class="col-2">
                                        {{ $playerScore->id }}
                                    </div>
                                    <div class="col-2">
                                        {{ $playerScore->name }}
                                    </div>
                                    <div class="col-2">
                                        {{ $playerScore->weight }}
                                    </div>
                                    <div class="col-2">
                                        {{ $playerScore->score }}
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
