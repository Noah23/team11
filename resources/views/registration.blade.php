@extends('layouts.app')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p style="float: left">Registratie</p>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{session('error')}}</li>
                                </ul>
                            </div>
                        @endif

                        <div id="register">
                            <i><p>Als u registreert voor de 999games evenement gaat u mee akkoord met de <a
                                        href="{{url('/voorwaarden')}}">voorwaarden</a></p></i>
                            <form autocomplete="off" method="post" action="{{url('/registerEvent')}}">
                                @csrf
                                    <div class="form-group">
                                        <select class="form-control" name="dropdown">
                                            @foreach($events as $event)
                                                <option value="{{ $event->id }}">{{ $event->eventname }}</option>
                                            @endforeach
                                        </select>
                                <div class="form-group">
                                    <label for="name">Volledige naam:</label>
                                    <input type="text" class="form-control" name="name" id="fullName" value="john doe"
                                           placeholder="John Doe..." required>
                                </div>
                                <div class="form-group">
                                    <label for="address">Adres:</label>
                                    <input type="text" class="form-control" name="address" id="address"
                                           value="jan luikenstraat" placeholder="Jan Luijkenstraat 13..." required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Telefoon nummer:</label>
                                    <input type="tel" class="form-control" name="phone" pattern="[0-9]{10}"
                                           placeholder="0612345684..." value="0624464024" required>
                                </div>
                                <div class="form-group">
                                    <label for="leeftijd">Leeftijd:</label>
                                    <input type="number" class="form-control" name="age">
                                    <small><i style="color: red;">Je moet minstens 10 jaar zijn om mee te
                                            doen</i></small>
                                </div>
                                        {{--<input type="hidden" value="{{$event->name}}" name="event">--}}
                                <input type="submit" value="Registreer" class="btn btn-primary"
                                       onclick="confirm('Weet u zeker als u voor dit evenement wilt registreren?')">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
