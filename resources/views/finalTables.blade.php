@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <i><small>Event nummer: {{Request::segment(2)}}</small></i> <br>
                        <p>1. Haal de top 16 spelers van de event. <i><small>Doe dit maar 1 keer</small></i></p>
                        <p>2. Deel de tafels in met Bereken willekeurig tafels knop. <i><small>Doe dit per ronde</small></i>
                        </p>
                        <button class="btn btn-primary" onclick="showTable()">Tafels</button>
                        <button class="btn btn-primary" onclick="showBranch()">Branch</button>
                        <button class="btn btn-primary"
                                onclick="window.location.href = '{{ url('/finalTop/' . Request::segment(2)) }}'">Haal
                            top 16 spelers
                        </button>
                        <button class="btn btn-primary"
                                onclick="window.location.href = '{{ url('/finalCalc/' . Request::segment(2)) }}'">
                            Bereken willekeurig Tafels
                        </button>
                    </div>
                    @if(session()->has('winner'))
                        @foreach(session('winner') as $winner)
                            <p>{{$winner->playerId}}</p>
                        @endforeach
                    @endif
                    <div id="branch">
                        <!-- start final 16 -->
                        <div id="16">
                            <h4 style="text-align: center; font-weight: bold; font-size: 30px; padding-top: 10px;">
                                Laatste 16</h4>
                            <table class="table table-reflow">
                                <tr>
                                    <th>Naam</th>
                                    <th>PlayerId</th>
                                    <th>Score</th>
                                </tr>
                                @foreach($ranking as $person)
                                    @if($person->round16 == true)
                                        <tr>
                                            <td>
                                                {{ $person->name }}
                                            </td>
                                            <td>{{$person->playerId}}</td>
                                            <td>{{$person->score}}</td>
                                        </tr>
                                    @endif
                                @endforeach

                            </table>
                        </div>
                        <!-- end final 16 -->
                        <!-- start final 8 -->
                        <div id="8">
                            <h4 style="text-align: center; font-weight: bold; font-size: 30px; padding-top: 10px;">
                                Laatste 8</h4>
                            <table class="table table-striped">
                                <tr>
                                    <th>Naam</th>
                                    <th>PlayerId</th>
                                    <th>Score</th>
                                </tr>
                                @foreach($ranking as $person)
                                    @if($person->round8 == true)
                                        <tr>
                                            <td>
                                                {{ $person->name }}
                                            </td>
                                            <td>{{$person->playerId}}</td>
                                            <td>{{$person->score}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                        <!-- end final 8 -->
                        <!-- start final 4 -->
                        <div id="8">
                            <h4 style="text-align: center; font-weight: bold; font-size: 30px; padding-top: 10px;">
                                Laatste 4</h4>
                            <table class="table table-striped">
                                <tr>
                                    <th>Naam</th>
                                    <th>PlayerId</th>
                                    <th>Score</th>
                                </tr>
                                @foreach($ranking as $person)
                                    @if($person->round4 == true)
                                        <tr>
                                            <td>
                                                {{ $person->name }}
                                            </td>
                                            <td>{{$person->playerId}}</td>
                                            <td>{{$person->score}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                        <!-- end final 4 -->
                        <!-- start final 2 -->
                        <div id="8">
                            <h4 style="text-align: center; font-weight: bold; font-size: 30px; padding-top: 10px;">
                                Laatste 2</h4>
                            <table class="table table-striped">
                                <tr>
                                    <th>Naam</th>
                                    <th>PlayerId</th>
                                    <th>Score</th>
                                </tr>
                                @foreach($ranking as $person)
                                    @if($person->round2 == true)
                                        <tr>
                                            <td>
                                                {{ $person->name }}
                                            </td>
                                            <td>{{$person->playerId}}</td>
                                            <td>{{$person->score}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                        <!-- end final 2 -->
                        <!-- start final 1 -->
                        <div id="1">
                            <h4 style="text-align: center; font-weight: bold; font-size: 30px; padding-top: 10px;">
                                Winaar</h4>
                            <table class="table table-striped">
                                <tr>
                                    <th>Naam</th>
                                    <th>PlayerId</th>
                                    <th>Score</th>
                                </tr>
                                @foreach($ranking as $person)
                                    @if($person->round1 == true)
                                        <tr>
                                            <td>
                                                {{ $person->name }}
                                            </td>
                                            <td>{{$person->playerId}}</td>
                                            <td>{{$person->score}}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        </div>
                        <!-- end final 1 -->
                    </div>
                    <div id="tables">
                        @for ($i = 1; $i <= $tableAmt; $i++)
                            <hr>
                            <h2>Table {{$i}}</h2>
                            <form action="{{ url('/finalWin')}}" method="POST">
                                @csrf
                                @method('patch')
                                @foreach ($ranking as $player)
                                    @if ($player->table == $i && $player->inComp == true)
                                        <input type="hidden" name="tableId" value="{{$player->table}}">
                                        <h3>Player {{$player->playerId}}</h3>
                                        <input type="number" name="score{{$player->id}}">
                                        <input type="hidden" name="{{$player->id}}" value="{{$player->id}}">
                                    @endif
                                @endforeach
                                <br>
                                <input type="submit">
                            </form>
                            <hr>
                        @endfor
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        showTable();


        function showTable() {
            document.getElementById('branch').style.display = "none";
            document.getElementById('tables').style.display = "block";
        }

        function showBranch() {
            document.getElementById('tables').style.display = "none";
            document.getElementById('branch').style.display = "block";
        }
    </script>
@endsection
