@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Profile</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(session()->has('message'))
                            <div class="bg-success text-white">{{session('message')}}</div>
                        @endif
                        <div>
                            @if (Auth::user()->type == 2)
                                Jou account type: <strong>Medewerker</strong>
                            @endif
                            @if (Auth::user()->type == 0)
                                Jou account type: <strong>User</strong>
                            @endif
                            @if (Auth::user()->type == 3)
                                Jou account type: <strong>Admin</strong>
                            @endif
                        </div>
                        <hr>
                        <div id="signedUp">
                            <h4>Inschrijvingen</h4>

                            @if ($results->isEmpty())
                                <p>Helaas, je bent nergens ingeschreven nog. Wil je dat wel doen? Dat kan <a
                                        href="{{url('/registers')}}">hier</a></p>
                            @endif
                            @foreach($results as $result)
                                <div class="subItem" style="background:rgba(255,255,255, 0.5);">
                                    <h4 id="name" style="text-align: center">{{$result->name}}</h4>
                                    <hr>
                                    <div id="inchrijvingen">
                                        <h5>Ingeschreven voor:</h5>
                                        {{-- @foreach ($inchrijvingen as $inchrijving)

                                        @endforeach --}}
                                        <ul>
                                            <li>{{$result->eventname}}</li>
                                        </ul>
                                    </div>
                                    <form action="unsubscribe" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="hidden" name="accountId"
                                               value="{{Crypt::encrypt($result->playerId)}}">
                                        <input type="hidden" name="id" value="{{Crypt::encrypt($result->id)}}">
                                        <input id="button" type="submit" class="btn btn-danger" value="Uitschrijven">
                                    </form>
                                    <br>

                                    <p style="float: left; padding: 0px; font-size: 8px;">ID:{{$result->id}}</p>


                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
