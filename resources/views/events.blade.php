@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 pt-2">
                <div class="card">
                    <div class="card-header">
                        Events
                    </div>
                        <table style="width:100%">
                            <tr>
                                <th>ID</th>
                                <th>Event</th>
                                <th>Bereken scores</th>
                                <th>Laatste 16</th>
                                <th>Score</th>
                            </tr>
                        @foreach($events as $event)
                        <tr>
                            <td> <div class="col-6">
                                    {{ $event->id }}
                                </div></td>
                            <td>
                                <div class="col-6">
                                    <a href="{{ route('rankings', ['event' => $event->id]) }}">
                                        {{ $event->eventname }}
                                    </a>
                                    <td>
                                <a href="{{ route('tables', ['event' => $event->id]) }}"
                                   class="btn btn-primary">
                                    Bereken tafels
                                </a>
                                </td>
                                <td>
                                    <a href="{{ url('finalTables/' . $event->id) }}" class="btn btn-primary">
                                        Naar de laatste 16
                                    </a>
                                </td>
                            <td>
                                <a href="{{url('edit/' . $event->id)}}" class="btn btn-primary">
                                    Invoer score
                                </a>
                            </td>
                        </tr>
                @endforeach
                    </table>
                    <a class="font-italic font-weight-bold">LET OP: Klik bij iedere invoer van de scores eerst op 'bereken tafels' en daarna 'invoer score'.</a>
                        </div>
                    </div>
                </div>
            </div>


@endsection

