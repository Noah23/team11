<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=MedievalSharp&display=swap" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>999Games</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/timer.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="icon" type="image/gif/png" href="{{asset('/images/title.png')}}" style="border-radius: 10px;">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
    <img src="{{asset("images/download.png")}}" id="home-header" href=" {{ url('/') }}">
        <nav  id="navbar" class="navbar sticky navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
            <a href="{{url('/')}}" style="float: left;" ><img src="{{asset('/images/logo.jpg')}}" style="width: 30px; height: 30px; border-radius: 10px; margin: 10px"></a>
                <a class="navbar-brand" href="{{ url('/') }}">
                    999Games
                </a>
                <a class="navbar-brand" href="{{ url('/registers') }}">
                    Registreer
                </a>
                <a class="navbar-brand" href="{{ route('events') }}">
                    Rankings
                </a>
{{--                <a class="navbar-brand" href="{{ route('editScore') }}">--}}
{{--                    Edit Score--}}
{{--                </a>--}}
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a style="float: right;" id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ url('profile') }}">
                                                Profile
                                        </a>
                                        @if (Auth::user()->type == 3)
                                            <a class="dropdown-item" href="{{ url('adminPanel') }}">
                                                Admin Panel
                                            </a>
                                        @endif
                                    <!-- Logout -->
                                    <div id="logout">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>

                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <div id="footer">
        <ul id="footer-bar">
            <li id="footer-item"><a href="/contact">Contact</a></li>
            <li id="footer-item"><a href="#">Voorwaarden</a></li>
            <li id="footer-item"><a href="#">Login</a></li>
        </ul>
    </div>

</body>

</html>
