<?php
/**
 * Created by PhpStorm.
 * User: rezasoekhai
 * Date: 13/12/2019
 * Time: 02:08
 */?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body">
                        <form action="{{url('/contact')}}" method="post">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label>Naam*</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Email*</label>
                                <input type="email" name="email" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Bericht*</label>
                                <textarea name="content" class="form-control" required></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary">Neem contact op!</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
