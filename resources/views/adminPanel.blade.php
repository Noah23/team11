@extends('layouts.app')

@section('content')
<script src="{{ asset('js/adminPanel.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <p style="float: left">Admin Panel</p>
                    <br>
                    <br>
                    <button type="button" class="btn btn-primary" onclick="showUsers()">Gebruikers</button>
                    <button type="button" class="btn btn-primary" onclick="showEvent()">Event Panel</button>
                    <button type="button" class="btn btn-primary" onclick="window.location.href = '{{ url('/registeredUsers') }}'">Inschrijvingen</button>
                    <button type="button" class="btn btn-primary" onclick="window.location.href = '{{ url('/confirm') }}'">Confirm</button>
                </div>
                <div id="event-page">
                    <div class="card-body">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{{session('success')}}</li>
                                </ul>
                            </div>
                        @endif
                    <form method="post" action="{{ url('createEvent') }}">
                        @csrf
                        <div class="input-group">
                            <input width="100px" type="text" name="eventname" class="form-control" placeholder="Event name" aria-label="" aria-describedby="basic-addon1">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Create event</button>
                            </div>
                        </div>
                    </form>
                </div>
                {{--end create event--}}
                    {{--start event view--}}
                <div class="card-body">
                <hr>
                    <!-- event table -->
                    <table style="width:100%">
                        <tr>
                            <th>ID</th>
                            <th>Naam</th>
                            <th></th>
                            <th>Datum</th>
                            <th style="text-align: center">Verwijderen</th>
                        </tr>
                        @foreach($events as $event)
                            <tr>
                                <td>{{$event->id}}</td>
                                <td><p class="type" data-toggle="modal" data-target="#updateEvent{{$event->id}}">{{$event->eventname}}</p></td>
                                <td>
                                    <div class="modal" id="updateEvent{{$event->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="input-group">
                                                        <form action="{{url('updateEvent')}}" method="post">
                                                            @csrf
                                                            @method('put')
                                                            <input type="hidden" name="id" value="{{ $event->id }}">
                                                            <input type="text" name="name" value="{{$event->eventname}}">
                                                            <input type="submit" value="Update" class="btn btn-primary">
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <td>{{$event->created_at}}</td>
                                <td>
                                    <div style="text-align: center;">
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeModal{{$event->id}}">
                                            X
                                        </button>
                                    </div>
                                </td>
                                <td>
                                    <div class="modal" id="removeModal{{$event->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Weet je zeker dat je <b>{{ $event->name }}</b> wil verwijderen?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    <form action="{{url('removeEvent')}}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <input type="hidden" name="id" value="{{$event->id}}">
                                                        <input type="submit" value="Verwijder" class="btn btn-danger">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div id="paginationEvents" style="text-align: center">
                        <div id="bar">
                            {{ $events->links() }}
                        </div>
                    </div>
                </div> <!-- this div closes event table!-->
            </div> <!-- this div closes event page!-->
                {{--end event --}}

                <div id="searchPage">
                    <div class="card-body">
                        <div class="adminSearchForm">
                            <form action="{{url('searchUser')}}" method="post">
                                @csrf
                                <div class="input-group">
                                    <input width="100px" type="text" name="search" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit">Search</button>
                                </div>
                            </form>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{session('error')}}</li>
                                </ul>
                            </div>
                        @endif
                        <!-- search -->
                        @if(session()->has('search'))
                        <table style="width:100%">
                                <tr>
                                    <th>ID</th>
                                    <th>Naam</th>
                                    <th>Email</th>
                                    <th>Type</th>
                                    <th>Datum</th>
                                    <th style="text-align: center">Verwijderen</th>
                                </tr>
                                @foreach(session('search') as $user)
                                    <tr>
                                        <td><p>{{$user->id}}</p></td>
                                        <td><p class="type" data-toggle="modal" data-target="#updateName{{$user->id}}">{{$user->name}}</p></td>
                                        <td><p class="type" data-toggle="modal" data-target="#updateEmail{{$user->id}}">{{$user->email}}</p></td>
                                        <td>
                                            @if ($user->type == 2)
                                                <p class="type"><a  data-toggle="modal" data-target="#upgradeType{{$user->id}}">Medewerker</a></p>
                                            @endif
                                            @if ($user->type == 0)
                                            <p class="type"><a  data-toggle="modal" data-target="#upgradeType{{$user->id}}">User</a></p>
                                            @endif
                                            @if ($user->type == 3)
                                            <p class="type"><a  data-toggle="modal" data-target="#upgradeType{{$user->id}}">Admin</a></p>
                                            @endif
                                        <!-- Modal upgrade type-->
                                        <div class="modal" id="upgradeType{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="input-group">
                                                                <form action="{{url('makeAdmin')}}" method="post">
                                                                    @csrf
                                                                    @method('put')
                                                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                                                    <input class="modal-button" type="submit" value="Maak admin" class="btn btn-primary">
                                                                </form>
                                                                <form action="{{url('makeEmployee')}}" method="post">
                                                                    @csrf
                                                                    @method('put')
                                                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                                                    <button class="mr-3" type="submit" class="btn btn-primary">Maak medewerker</button>
                                                                </form>
                                                                <form action="{{url('makeUser')}}" method="post">
                                                                    @csrf
                                                                    @method('put')
                                                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                                                    <button class="modal-button" type="submit" class="btn btn-primary">Maak users</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        <!-- Modal upgrade name-->
                                        <div class="modal" id="updateName{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-group">
                                                            <form action="{{url('updateName')}}" method="post">
                                                                @csrf
                                                                @method('put')
                                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                                <input type="text" name="name" value="{{$user->name}}">
                                                                <input type="submit" value="Update" class="btn btn-primary">
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        <!-- Modal upgrade email-->
                                        <div class="modal" id="updateEmail{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-group">
                                                            <form action="{{url('updateEmail')}}" method="post">
                                                                @csrf
                                                                @method('put')
                                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                                <input type="text" name="email" value="{{$user->email}}">
                                                                <input type="submit" value="Update" class="btn btn-primary">
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        </td>
                                        <td>{{$user->created_at}}</td>
                                        <td>
                                        <!-- Remove User Modal -->
                                        <div class="modal" id="removeModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Weet je zeker dat je <b>{{ $user->name }}</b> wil verwijderen?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                        <form action="{{url('removeUser')}}" method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <input type="hidden" name="id" value="{{$user->id}}">
                                                            <input type="submit" value="Verwijderen" class="btn btn-danger">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        <div style="text-align: center;">
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeModal{{$user->id}}">
                                                    X
                                            </button>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                        <hr>
                        @if(!session('search'))
                            <!-- normal table -->
                            <div class="table-responsive">
                            <table style="width:100%">
                                <tr>
                                    <th>ID</th>
                                    <th>Naam</th>
                                    <th>Email</th>
                                    <th>Type</th>
                                    <th>Datum</th>
                                    <th style="text-align: center">Verwijderen</th>
                                </tr>
                                @foreach($users as $user)
                                    <tr>
                                        <td><p>{{$user->id}}</p></td>
                                        <td><p class="type" data-toggle="modal" data-target="#updateName{{$user->id}}">{{$user->name}}</p></td>
                                        <td><p class="type" data-toggle="modal" data-target="#updateEmail{{$user->id}}">{{$user->email}}</p></td>
                                        <td>
                                            @if ($user->type == 2)
                                                <p class="type"><a  data-toggle="modal" data-target="#upgradeType{{$user->id}}">Medewerker</a></p>
                                            @endif
                                            @if ($user->type == 0)
                                            <p class="type"><a  data-toggle="modal" data-target="#upgradeType{{$user->id}}">User</a></p>
                                            @endif
                                            @if ($user->type == 3)
                                            <p class="type"><a  data-toggle="modal" data-target="#upgradeType{{$user->id}}">Admin</a></p>
                                            @endif
                                        <!-- Modal upgrade type-->
                                        <div class="modal" id="upgradeType{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="input-group row justify-content-center">
                                                                <form action="{{url('makeAdmin')}}" method="post">
                                                                    @csrf
                                                                    @method('put')
                                                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                                                    <input type="submit" value="Maak admin" class="btn btn-primary mr-2">
                                                                </form>
                                                                <form action="{{url('makeEmployee')}}" method="post">
                                                                    @csrf
                                                                    @method('put')
                                                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                                                    <button type="submit" class="btn btn-primary mr-2">Maak medewerker</button>
                                                                </form>
                                                                <form action="{{url('makeUser')}}" method="post">
                                                                    @csrf
                                                                    @method('put')
                                                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                                                    <button type="submit" class="btn btn-primary">Maak users</button>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        <!-- Modal upgrade name-->
                                        <div class="modal" id="updateName{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-group">
                                                            <form action="{{url('updateName')}}" method="post">
                                                                @csrf
                                                                @method('put')
                                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                                <input type="text" name="name" value="{{$user->name}}">
                                                                <input type="submit" value="Update" class="btn btn-primary">
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        <!-- Modal upgrade email-->
                                        <div class="modal" id="updateEmail{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="input-group">
                                                            <form action="{{url('updateEmail')}}" method="post">
                                                                @csrf
                                                                @method('put')
                                                                <input type="hidden" name="id" value="{{ $user->id }}">
                                                                <input type="text" name="email" value="{{$user->email}}">
                                                                <input type="submit" value="Update" class="btn btn-primary">
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        </td>
                                        <td>{{$user->created_at}}</td>
                                        <td>
                                        <!-- Remove User Modal -->
                                        <div class="modal" id="removeModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Weet je zeker dat je <b>{{ $user->name }}</b> wil verwijderen?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten</button>
                                                        <form action="{{url('removeUser')}}" method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <input type="hidden" name="id" value="{{$user->id}}">
                                                            <input type="submit" value="Verwijderen" class="btn btn-danger">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                        <div style="text-align: center;">
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#removeModal{{$user->id}}">
                                                    X
                                            </button>
                                        </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            </div>
                            <div id="pagination" style="text-align: center">
                                <div id="bar">
                                    {{ $users->links() }}
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
</style>
<script>


    function hideAll() {
        document.getElementById('event-page').style.display = "none";
        document.getElementById('searchPage').style.display = "none";
    }

    function showUsers() {
        hideAll();
        document.getElementById('searchPage').style.display = "block";
    }

    function showEvent() {
        hideAll();
        document.getElementById('event-page').style.display = "block";
    }

    showUsers();

<?php
   if (isset($_GET['cre'])) {
       echo "showEvent();";
   }
?>

</script>

@endsection
