@extends('layouts.app')

@section('content')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p style="float: left">Geregistreerde gebruikers</p>
                        <br>
                        <br>
                        <button type="button" class="btn btn-primary"
                                onclick="window.location.href = '{{ url('/adminPanel') }}'">Gebruikers
                        </button>
                        <button type="button" class="btn btn-primary"
                                onclick="window.location.href = '{{ url('/adminPanel') . '?cre=1' }}';">Creëer
                        </button>
                        <button type="button" class="btn btn-primary"
                                onclick="window.location.href = '{{ url('/registeredUsers') }}'">Inschrijvingen
                        </button>
                        <button style="float: right" type="button" class="btn btn-danger" data-toggle="modal"
                                data-target="#exampleModal">
                            Reset Inschrijvingen
                        </button>
                    </div>
                    <!-- Modal -->
                    <div class="modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                         aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Weet je zeker dat je de tabel wil resetten?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Sluiten
                                    </button>
                                    <form action="{{url('resetRegistration')}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <input type="submit" value="Reset" class="btn btn-danger">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end -->
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                <ul>
                                    <li>{{session('error')}}</li>
                                </ul>
                            </div>
                        @endif

                        <div class="col-12 pb-3">
                            <form action="/searchUser" method="post">
                                @csrf
                                <div class="input-group">
                                    <input width="100px" type="text" name="search" class="form-control" placeholder=""
                                           aria-label="" aria-describedby="basic-addon1">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <table style="width:100%">
                            <tr>
                                <th>ID</th>
                                <th>Naam</th>
                                <th>Telefoon</th>
                                <th>Address</th>
                                <th>Leeftijd</th>
                                <th>AccountID</th>
                                <th style="text-align: center"></th>
                            </tr>
                            @foreach($registered as $register)
                                <tr>
                                    <td>{{ $register->id }}</td>
                                    <td>{{$register->name}}</td>
                                    <td>{{$register->number}}</td>
                                    <td>{{$register->address}}</td>
                                    <td>{{$register->age}}</td>
                                    <td>{{$register->accountId}}</td>

                                    <td>
                                        <!-- Modal -->


                                        <div style="text-align: center">

                                            <a href="{{ route('editScore', ['player' => $register->id]) }}"
                                               class="btn btn-primary">
                                                Update Score
                                            </a>
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                    data-target="#removeModal">
                                                X
                                            </button>
                                        </div>


                                        <div class="modal" id="removeModal" tabindex="-1" role="dialog"
                                             aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Bevestiging</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Weet je zeker dat je <b>{{$register->name}}</b> wil verwijderen?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Sluiten
                                                        </button>
                                                        <form action="{{ url('removeFromRegister') }}" method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <input type="hidden" name="id"
                                                                   value="{{Crypt::encrypt($register->id)}}">
                                                            <input type="hidden" name="playerId" value="{{ Crypt::encrypt($register->playerId) }}">
                                                            <center><input type="submit" value="Verwijder"
                                                                           class="btn btn-danger"></center>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end -->
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                        <div id="pagination" style="text-align: center">
                            <div id="bar">
                                {{--                                {{ $registered->links() }}--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
    </style>
@endsection
