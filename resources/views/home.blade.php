@extends('layouts.app')

@section('content')
<link href="#">
<link rel="stylesheet" href="{{ asset('css/home.css') }}">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <br>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        
                    <div id="home-content">
                        <div id="home-main">
                            <h1 class="home-title">999Games - Evenement</h1>
                            {{-- <p>Dit is de unofficiele website van de 999Games Carcassonne evenement. Meld je hier aan voor een evenement van 999Games. Speel met honderden mensen tegelijk.</p> --}}
                        </div>
                        <hr>
                        <div id="main2">
                            <img id="img1" src="{{ asset("images/im.png") }}">
                            <h3 class="sub-title">Meld je aan...</h3>
                            <p>Je kunt je aanmelden om eerst <a href="{{url("/login")}}">in te loggen</a> op onze website. Daarna kan je registreren naar het evenement. Je kunt maximaal 5 mensen registreren, waarbij de minimum leeftijd 10 jaar is.</p>
                            <h3 class="sub-title">Het spel...</h3>
                            <p>Het spel
                                    Klaus-Jürgen Wrede, de auteur van Carcassonne, bracht eind vorige eeuw een bezoek aan de gelijknamige Franse stad. Hij was daar om inspiratie op te doen voor het schrijven van een roman.
                                    
                                    En geïnspireerd raakte hij zeker, maar niet voor zijn boek. Klaus-Jürgen had meerdere ijzers in het vuur, want hij hield zich als hobbyist bezig met het ontwerpen van bordspellen. De historische stad en omgeving brachten hem het perfecte thema voor zijn te ontwerpen spel; slechts een half jaar later was Carcassonne klaar en getest. Zijn spelconcept werd meteen opgepikt en in de herfst van 2000 werd het gepresenteerd op de spellenbeurs Spiel in Duitsland.
                                    
                                    Een jaar later won het op diezelfde beurs de prijs voor Spiel des Jahres, gevolgd door de Deutsche Spielepreis. Deze Duitse dubbel zou het begin betekenen van de zegetocht van Carcassonne. Het spel is met prijzen overladen, kent tal van uitbreidingen en wordt nog altijd veel verkocht.</p>
                            <img id="img2" src="{{ asset("images/car2.png") }}">
                            <h3 class="sub-title">Carcassonne...</h3>
                            <p>Met het spelen van je eerste potje Carcassonne valt direct op hoe eenvoudig het spelmechanisme is. Je trekt een tegel, legt deze aan de tegels die er al liggen en zet er eventueel een meeple er op. Een kind kan de was doen. In de kern lijkt Carcassonne dus uiterst simpel en kan daarom ook door iedereen, van jong tot oud, gespeeld worden. Maar in al zijn simpelheid schuilt er een verdraaid slim spelsysteem in Carcassonne waar je met de juiste strategie het spel naar je toe kan trekken. Dus of je nu een doorgewinterde tacticus of een beginnende speler bent, Carcassonne biedt voor elk wat wils.</p>
                        </div>
                        <hr>
                        <h3></h3>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
