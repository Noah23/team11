<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RegisterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('registers')->insert([
            'number' => Int::random(10),
            'name' => Str::random(10),
            'accountId' => Int::random(100),
            'address' => Str::random(100),
            'eventId' => 1,
            'age' => Int::random(99)
        ]);
    }
}
