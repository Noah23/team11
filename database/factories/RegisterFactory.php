<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Register;
use Faker\Generator as Faker;

$factory->define(Register::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'number' => 3939393,
        'address' => $faker->address,
        'playerId' => $faker->regexify('[1-9]{8}'), // password
        'age' => $faker->regexify('[1-9]{2}'),
        'eventId' => 1
    ];
});
