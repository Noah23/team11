<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayersScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_scores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('score')->default(0);
            $table->integer('RankingScore')->default(0);
            $table->integer('totalScore')->default(0);
            $table->string('playerId');
            $table->integer('tableNumber')->default(0);
            $table->integer('eventId');
            $table->integer('weight')->default(0);
            $table->string('tournamentRound')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_scores');
    }
}
