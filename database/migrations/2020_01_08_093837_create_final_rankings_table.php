<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinalRankingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('final_rankings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('eventId');
            $table->integer('playerId');
            $table->integer('score');
            $table->integer('weight');
            $table->integer('table')->default(0);
            $table->boolean('inComp')->default(true); // in competetion 
            $table->boolean('round16')->default(true);
            $table->boolean('round8')->default(false);
            $table->boolean('round4')->default(false);
            $table->integer('round2')->default(false);
            $table->integer('round1')->default(false);
            $table->integer('playerNumber')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('final_rankings');
    }
}
